const http = require('http');
const fs = require('fs');
const path = require('path');

const server = http.createServer((req, res) => {
    // Obtenha o caminho absoluto do arquivo solicitado
    const filePath = path.join(__dirname, 'public', req.url === '/' ? 'index.html' : req.url);

    // Verifique se o arquivo existe
    fs.access(filePath, fs.constants.F_OK, (err) => {
        if (err) {
            // Arquivo não encontrado
            res.writeHead(404, {'Content-Type': 'text/plain'});
            res.end('File not found');
            return;
        }

        // Leia o conteúdo do arquivo
        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
                // Erro ao ler o arquivo
                res.writeHead(500, {'Content-Type': 'text/plain'});
                res.end('Internal Server Error');
                return;
            }

            // Responda com o conteúdo do arquivo
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.end(data);
        });
    });
});

const PORT = process.env.PORT || 3000;

server.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});